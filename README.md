Сразу извиняюсь за грязь с .replace :) 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
train_data = pd.read_csv('ks_train.csv')
test_data = pd.read_csv('ks_test.csv')



names_X = train_data['category'].unique()
names_X2 = test_data['category'].unique()
for i in range (len(names_X)):
    (train_data['category'].unique())[i] = i
for i in range (len(names_y)):
    names[i] = i
train_data['category'].unique()[1]



train_data['launched'] = pd.to_datetime(train_data['launched'])
test_data['launched'] = pd.to_datetime(test_data['launched'])

train_data['deadline'] = pd.to_datetime(train_data['launched'])
test_data['deadline'] = pd.to_datetime(test_data['launched'])

train_data['launched'] = train_data['launched'].astype('int64')
test_data['launched'] = train_data['launched'].astype('int64')

train_data['deadline'] = train_data['deadline'].astype('int64')
test_data['deadline'] = train_data['deadline'].astype('int64')



train_data['state'].replace(to_replace='successful', value=1, inplace=True)
train_data['state'].replace(to_replace='failed', value=0, inplace=True)

test_data['state'].replace(to_replace='successful', value=1, inplace=True)
test_data['state'].replace(to_replace='failed', value=0, inplace=True)

train_data['main_category'].replace(to_replace='Film & Video', value=1, inplace=True)
test_data['main_category'].replace(to_replace='Film & Video', value=1, inplace=True)

train_data['main_category'].replace(to_replace='Music', value=2, inplace=True)
test_data['main_category'].replace(to_replace='Music', value=2, inplace=True)

train_data['main_category'].replace(to_replace='Publishing', value=3, inplace=True)
test_data['main_category'].replace(to_replace='Publishing', value=3, inplace=True)

train_data['main_category'].replace(to_replace='Games', value=4, inplace=True)
test_data['main_category'].replace(to_replace='Games', value=4, inplace=True)

train_data['main_category'].replace(to_replace='Art', value=5, inplace=True)
test_data['main_category'].replace(to_replace='Art', value=5, inplace=True)

train_data['main_category'].replace(to_replace='Technology', value=6, inplace=True)
test_data['main_category'].replace(to_replace='Technology', value=6, inplace=True)

train_data['main_category'].replace(to_replace='Design', value=7, inplace=True)
test_data['main_category'].replace(to_replace='Design', value=7, inplace=True)

train_data['main_category'].replace(to_replace='Food', value=8, inplace=True)
test_data['main_category'].replace(to_replace='Food', value=8, inplace=True)

train_data['main_category'].replace(to_replace='Fashion', value=9, inplace=True)
test_data['main_category'].replace(to_replace='Fashion', value=9, inplace=True)

train_data['main_category'].replace(to_replace='Comics', value=10, inplace=True)
test_data['main_category'].replace(to_replace='Comics', value=10, inplace=True)

train_data['main_category'].replace(to_replace='Theater', value=10, inplace=True)
test_data['main_category'].replace(to_replace='Theater', value=10, inplace=True)

train_data['main_category'].replace(to_replace='Photography', value=11, inplace=True)
test_data['main_category'].replace(to_replace='Photography', value=11, inplace=True)

train_data['main_category'].replace(to_replace='Crafts', value=12, inplace=True)
test_data['main_category'].replace(to_replace='Crafts', value=12, inplace=True)

train_data['main_category'].replace(to_replace='Journalism', value=13, inplace=True)
test_data['main_category'].replace(to_replace='Journalism', value=13, inplace=True)

train_data['main_category'].replace(to_replace='Dance', value=14, inplace=True)
test_data['main_category'].replace(to_replace='Dance', value=14, inplace=True)

train_data['currency'].replace(to_replace='USD', value=1, inplace=True)
test_data['currency'].replace(to_replace='USD', value=1, inplace=True)

train_data['currency'].replace(to_replace='GBP', value=2, inplace=True)
test_data['currency'].replace(to_replace='GBP', value=2, inplace=True)

train_data['currency'].replace(to_replace='EUR', value=3, inplace=True)
test_data['currency'].replace(to_replace='EUR', value=3, inplace=True)

train_data['currency'].replace(to_replace='CAD', value=3, inplace=True)
test_data['currency'].replace(to_replace='CAD', value=3, inplace=True)

train_data['currency'].replace(to_replace='AUD', value=4, inplace=True)
test_data['currency'].replace(to_replace='AUD', value=4, inplace=True)

train_data['currency'].replace(to_replace='SEK', value=5, inplace=True)
test_data['currency'].replace(to_replace='SEK', value=5, inplace=True)

train_data['currency'].replace(to_replace='MXN', value=6, inplace=True)
test_data['currency'].replace(to_replace='MXN', value=6, inplace=True)

train_data['currency'].replace(to_replace='NZD', value=7, inplace=True)
test_data['currency'].replace(to_replace='NZD', value=7, inplace=True)

train_data['currency'].replace(to_replace='DKK', value=8, inplace=True)
test_data['currency'].replace(to_replace='DKK', value=8, inplace=True)

train_data['currency'].replace(to_replace='CHF', value=9, inplace=True)
test_data['currency'].replace(to_replace='CHF', value=9, inplace=True)

train_data['currency'].replace(to_replace='NOK', value=10, inplace=True)
test_data['currency'].replace(to_replace='NOK', value=10, inplace=True)

train_data['currency'].replace(to_replace='HKD', value=11, inplace=True)
test_data['currency'].replace(to_replace='HKD', value=11, inplace=True)

train_data['currency'].replace(to_replace='SGD', value=12, inplace=True)
test_data['currency'].replace(to_replace='SGD', value=12, inplace=True)

train_data['currency'].replace(to_replace='JPY', value=13, inplace=True)
test_data['currency'].replace(to_replace='JPY', value=13, inplace=True)

train_data['currency'].replace(to_replace='JPY', value=13, inplace=True)
test_data['currency'].replace(to_replace='JPY', value=13, inplace=True)

train_data['country'].replace(to_replace='US', value=1, inplace=True)
test_data['country'].replace(to_replace='US', value=1, inplace=True)

train_data['country'].replace(to_replace='GB', value=2, inplace=True)
test_data['country'].replace(to_replace='GB', value=2, inplace=True)

train_data['country'].replace(to_replace='CA', value=3, inplace=True)
test_data['country'].replace(to_replace='CA', value=3, inplace=True)

train_data['country'].replace(to_replace='AU', value=4, inplace=True)
test_data['country'].replace(to_replace='AU', value=4, inplace=True)

train_data['country'].replace(to_replace='DE', value=5, inplace=True)
test_data['country'].replace(to_replace='DE', value=5, inplace=True)

train_data['country'].replace(to_replace='IT', value=6, inplace=True)
test_data['country'].replace(to_replace='IT', value=6, inplace=True)

train_data['country'].replace(to_replace='FR', value=6, inplace=True)
test_data['country'].replace(to_replace='FR', value=6, inplace=True)

train_data['country'].replace(to_replace='NL', value=7, inplace=True)
test_data['country'].replace(to_replace='NL', value=7, inplace=True)

train_data['country'].replace(to_replace='ES', value=8, inplace=True)
test_data['country'].replace(to_replace='ES', value=8, inplace=True)

train_data['country'].replace(to_replace='SE', value=9, inplace=True)
test_data['country'].replace(to_replace='SE', value=9, inplace=True)

train_data['country'].replace(to_replace='MX', value=10, inplace=True)
test_data['country'].replace(to_replace='MX', value=10, inplace=True)

train_data['country'].replace(to_replace='NZ', value=11, inplace=True)
test_data['country'].replace(to_replace='NZ', value=11, inplace=True)

train_data['country'].replace(to_replace='DK', value=12, inplace=True)
test_data['country'].replace(to_replace='DK', value=12, inplace=True)

train_data['country'].replace(to_replace='IE', value=13, inplace=True)
test_data['country'].replace(to_replace='IE', value=13, inplace=True)

train_data['country'].replace(to_replace='CH', value=14, inplace=True)
test_data['country'].replace(to_replace='CH', value=14, inplace=True)

train_data['country'].replace(to_replace='NO', value=15, inplace=True)
test_data['country'].replace(to_replace='NO', value=15, inplace=True)

train_data['country'].replace(to_replace='AT', value=16, inplace=True)
test_data['country'].replace(to_replace='AT', value=16, inplace=True)

train_data['country'].replace(to_replace='HK', value=17, inplace=True)
test_data['country'].replace(to_replace='HK', value=17, inplace=True)

train_data['country'].replace(to_replace='BE', value=18, inplace=True)
test_data['country'].replace(to_replace='BE', value=18, inplace=True)

train_data['country'].replace(to_replace='SG', value=19, inplace=True)
test_data['country'].replace(to_replace='SG', value=19, inplace=True)

train_data['country'].replace(to_replace='N,0"', value=20, inplace=True)
test_data['country'].replace(to_replace='N,0"', value=20, inplace=True)

train_data['country'].replace(to_replace='LU', value=21, inplace=True)
test_data['country'].replace(to_replace='LU', value=21, inplace=True)

train_data['country'].replace(to_replace='JP', value=22, inplace=True)
test_data['country'].replace(to_replace='JP', value=22, inplace=True)

train_data['deadline'] = np.log(train_data['deadline'].values)
test_data['deadline'] = np.log(test_data['deadline'].values)

train_data['launched'] = np.log(train_data['launched'].values)
test_data['launched'] = np.log(test_data['launched'].values)

train_data['category'].replace(to_replace='Comics', value=1, inplace=True)
test_data['category'].replace(to_replace='Comics', value=1, inplace=True)

train_data['category'].replace(to_replace='Apps', value=2, inplace=True)
test_data['category'].replace(to_replace='Apps', value=2, inplace=True)

train_data['category'].replace(to_replace='Rock', value=3, inplace=True)
test_data['category'].replace(to_replace='Rock', value=3, inplace=True)

train_data['category'].replace(to_replace='Plays', value=4, inplace=True)
test_data['category'].replace(to_replace='Plays', value=4, inplace=True)

train_data['category'].replace(to_replace='Product Design', value=5, inplace=True)
test_data['category'].replace(to_replace='Product Design', value=5, inplace=True)

train_data['category'].replace(to_replace='Fiction', value=6, inplace=True)
test_data['category'].replace(to_replace='Fiction', value=6, inplace=True)

train_data['category'].replace(to_replace='Metal', value=7, inplace=True)
test_data['category'].replace(to_replace='Metal', value=7, inplace=True)

train_data['category'].replace(to_replace='Painting', value=8, inplace=True)
test_data['category'].replace(to_replace='Painting', value=8, inplace=True)

train_data['category'].replace(to_replace='Video Art', value=9, inplace=True)
test_data['category'].replace(to_replace='Video Art', value=9, inplace=True)

train_data['category'].replace(to_replace='Video Games', value=10, inplace=True)
test_data['category'].replace(to_replace='Video Games', value=10, inplace=True)

train_data['category'].replace(to_replace='Music', value=11, inplace=True)
test_data['category'].replace(to_replace='Music', value=11, inplace=True)

train_data['category'].replace(to_replace='Comedy', value=12, inplace=True)
test_data['category'].replace(to_replace='Comedy', value=12, inplace=True)

train_data['category'].replace(to_replace='Fashion', value=13, inplace=True)
test_data['category'].replace(to_replace='Fashion', value=13, inplace=True)

train_data['category'].replace(to_replace="Children's Books", value=14, inplace=True)
test_data['category'].replace(to_replace="Children's Books", value=14, inplace=True)

train_data['category'].replace(to_replace='Jewelry', value=15, inplace=True)
test_data['category'].replace(to_replace='Jewelry', value=15, inplace=True)

train_data['category'].replace(to_replace='Technology', value=16, inplace=True)
test_data['category'].replace(to_replace='Technology', value=16, inplace=True)

train_data['category'].replace(to_replace='Film & Video', value=17, inplace=True)
test_data['category'].replace(to_replace='Film & Video', value=17, inplace=True)

train_data['category'].replace(to_replace='Food', value=18, inplace=True)
test_data['category'].replace(to_replace='Food', value=18, inplace=True)

train_data['category'].replace(to_replace='Photography', value=19, inplace=True)
test_data['category'].replace(to_replace='Photography', value=19, inplace=True)

train_data['category'].replace(to_replace='Dance', value=20, inplace=True)
test_data['category'].replace(to_replace='Dance', value=20, inplace=True)

train_data['category'].replace(to_replace='Mixed Media', value=21, inplace=True)
test_data['category'].replace(to_replace='Mixed Media', value=21, inplace=True)

train_data['category'].replace(to_replace='Tabletop Games', value=22, inplace=True)
test_data['category'].replace(to_replace='Tabletop Games', value=22, inplace=True)

train_data['category'].replace(to_replace='World Music', value=23, inplace=True)
test_data['category'].replace(to_replace='World Music', value=23, inplace=True)

train_data['category'].replace(to_replace='Hip-Hop', value=24, inplace=True)
test_data['category'].replace(to_replace='Hip-Hop', value=24, inplace=True)

train_data['category'].replace(to_replace='Theater', value=25, inplace=True)
test_data['category'].replace(to_replace='Theater', value=25, inplace=True)

train_data['category'].replace(to_replace='Music Videos', value=26, inplace=True)
test_data['category'].replace(to_replace='Music Videos', value=26, inplace=True)

train_data['category'].replace(to_replace='Playing Cards', value=27, inplace=True)
test_data['category'].replace(to_replace='Playing Cards', value=27, inplace=True)

train_data['category'].replace(to_replace='Apparel', value=28, inplace=True)
test_data['category'].replace(to_replace='Apparel', value=28, inplace=True)

train_data['category'].replace(to_replace='Conceptual Art', value=29, inplace=True)
test_data['category'].replace(to_replace='Conceptual Art', value=29, inplace=True)

train_data['category'].replace(to_replace='Publishing', value=30, inplace=True)
test_data['category'].replace(to_replace='Publishing', value=30, inplace=True)

train_data['category'].replace(to_replace='Classical Music', value=31, inplace=True)
test_data['category'].replace(to_replace='Classical Music', value=31, inplace=True)

train_data['category'].replace(to_replace='Electronic Music', value=32, inplace=True)
test_data['category'].replace(to_replace='Electronic Music', value=32, inplace=True)

train_data['category'].replace(to_replace='Food Trucks', value=33, inplace=True)
test_data['category'].replace(to_replace='Food Trucks', value=33, inplace=True)

train_data['category'].replace(to_replace='Public Art', value=34, inplace=True)
test_data['category'].replace(to_replace='Public Art', value=34, inplace=True)

train_data['category'].replace(to_replace='Crafts', value=35, inplace=True)
test_data['category'].replace(to_replace='Crafts', value=35, inplace=True)

train_data['category'].replace(to_replace='Software', value=36, inplace=True)
test_data['category'].replace(to_replace='Software', value=36, inplace=True)

train_data['category'].replace(to_replace='Shorts', value=37, inplace=True)
test_data['category'].replace(to_replace='Shorts', value=37, inplace=True)

train_data['category'].replace(to_replace='Restaurants', value=38, inplace=True)
test_data['category'].replace(to_replace='Restaurants', value=38, inplace=True)

train_data['category'].replace(to_replace='Web', value=39, inplace=True)
test_data['category'].replace(to_replace='Web', value=39, inplace=True)

train_data['category'].replace(to_replace='Hardware', value=40, inplace=True)
test_data['category'].replace(to_replace='Hardware', value=40, inplace=True)

train_data['category'].replace(to_replace='People', value=41, inplace=True)
test_data['category'].replace(to_replace='People', value=41, inplace=True)

train_data['category'].replace(to_replace='Experimental', value=42, inplace=True)
test_data['category'].replace(to_replace='Experimental', value=42, inplace=True)

train_data['category'].replace(to_replace='Documentary', value=43, inplace=True)
test_data['category'].replace(to_replace='Documentary', value=43, inplace=True)

train_data['category'].replace(to_replace='Gadgets', value=44, inplace=True)
test_data['category'].replace(to_replace='Gadgets', value=44, inplace=True)

train_data['category'].replace(to_replace='Print', value=45, inplace=True)
test_data['category'].replace(to_replace='Print', value=45, inplace=True)

train_data['category'].replace(to_replace='Animation', value=46, inplace=True)
test_data['category'].replace(to_replace='Animation', value=46, inplace=True)

train_data['category'].replace(to_replace='Drama', value=47, inplace=True)
test_data['category'].replace(to_replace='Drama', value=47, inplace=True)

train_data['category'].replace(to_replace='Webseries', value=48, inplace=True)
test_data['category'].replace(to_replace='Webseries', value=48, inplace=True)

train_data['category'].replace(to_replace='Glass', value=49, inplace=True)
test_data['category'].replace(to_replace='Glass', value=49, inplace=True)

train_data['category'].replace(to_replace='Architecture', value=50, inplace=True)
test_data['category'].replace(to_replace='Architecture', value=50, inplace=True)

train_data['category'].replace(to_replace='Accessories', value=51, inplace=True)
test_data['category'].replace(to_replace='Accessories', value=51, inplace=True)

train_data['category'].replace(to_replace='Calendars', value=52, inplace=True)
test_data['category'].replace(to_replace='Calendars', value=52, inplace=True)

train_data['category'].replace(to_replace='Poetry', value=53, inplace=True)
test_data['category'].replace(to_replace='Poetry', value=53, inplace=True)

train_data['category'].replace(to_replace='Illustration', value=54, inplace=True)
test_data['category'].replace(to_replace='Illustration', value=54, inplace=True)

train_data['category'].replace(to_replace='Embroidery', value=55, inplace=True)
test_data['category'].replace(to_replace='Embroidery', value=55, inplace=True)

train_data['category'].replace(to_replace='Pop', value=56, inplace=True)
test_data['category'].replace(to_replace='Pop', value=56, inplace=True)

train_data['category'].replace(to_replace='Webcomics', value=57, inplace=True)
test_data['category'].replace(to_replace='Webcomics', value=57, inplace=True)

train_data['category'].replace(to_replace='Audio', value=58, inplace=True)
test_data['category'].replace(to_replace='Audio', value=58, inplace=True)

train_data['category'].replace(to_replace='Nonfiction', value=59, inplace=True)
test_data['category'].replace(to_replace='Nonfiction', value=59, inplace=True)

train_data['category'].replace(to_replace='Indie Rock', value=60, inplace=True)
test_data['category'].replace(to_replace='Indie Rock', value=60, inplace=True)

train_data['category'].replace(to_replace='Performance Art', value=61, inplace=True)
test_data['category'].replace(to_replace='Performance Art', value=61, inplace=True)

train_data['category'].replace(to_replace='Spaces', value=62, inplace=True)
test_data['category'].replace(to_replace='Spaces', value=62, inplace=True)

train_data['category'].replace(to_replace='Mobile Games', value=63, inplace=True)
test_data['category'].replace(to_replace='Mobile Games', value=63, inplace=True)

train_data['category'].replace(to_replace='Sculpture', value=64, inplace=True)
test_data['category'].replace(to_replace='Sculpture', value=64, inplace=True)

train_data['category'].replace(to_replace='Small Batch', value=65, inplace=True)
test_data['category'].replace(to_replace='Small Batch', value=65, inplace=True)

train_data['category'].replace(to_replace='Interactive Design', value=66, inplace=True)
test_data['category'].replace(to_replace='Interactive Design', value=66, inplace=True)

train_data['category'].replace(to_replace='Thrillers', value=67, inplace=True)
test_data['category'].replace(to_replace='Thrillers', value=67, inplace=True)

train_data['category'].replace(to_replace='Makerspaces', value=68, inplace=True)
test_data['category'].replace(to_replace='Makerspaces', value=68, inplace=True)

train_data['category'].replace(to_replace='Art Books', value=69, inplace=True)
test_data['category'].replace(to_replace='Art Books', value=69, inplace=True)

train_data['category'].replace(to_replace='Zines', value=70, inplace=True)
test_data['category'].replace(to_replace='Zines', value=70, inplace=True)

train_data['category'].replace(to_replace='Comic Books', value=71, inplace=True)
test_data['category'].replace(to_replace='Comic Books', value=71, inplace=True)

train_data['category'].replace(to_replace='Games', value=72, inplace=True)
test_data['category'].replace(to_replace='Games', value=72, inplace=True)

train_data['category'].replace(to_replace='Photobooks', value=73, inplace=True)
test_data['category'].replace(to_replace='Photobooks', value=73, inplace=True)

train_data['category'].replace(to_replace="Farmer's Markets", value=74, inplace=True)
test_data['category'].replace(to_replace="Farmer's Markets", value=74, inplace=True)

train_data['category'].replace(to_replace='Design', value=75, inplace=True)
test_data['category'].replace(to_replace='Design', value=75, inplace=True)

train_data['category'].replace(to_replace='Narrative Film', value=76, inplace=True)
test_data['category'].replace(to_replace='Narrative Film', value=76, inplace=True)

train_data['category'].replace(to_replace='Radio & Podcasts', value=77, inplace=True)
test_data['category'].replace(to_replace='Radio & Podcasts', value=77, inplace=True)

train_data['category'].replace(to_replace='Digital Art', value=78, inplace=True)
test_data['category'].replace(to_replace='Digital Art', value=78, inplace=True)

train_data['category'].replace(to_replace='Anthologies', value=79, inplace=True)
test_data['category'].replace(to_replace='Anthologies', value=79, inplace=True)

train_data['category'].replace(to_replace='Translations', value=80, inplace=True)
test_data['category'].replace(to_replace='Translations', value=80, inplace=True)

train_data['category'].replace(to_replace='Footwear', value=81, inplace=True)
test_data['category'].replace(to_replace='Footwear', value=81, inplace=True)

train_data['category'].replace(to_replace='R&B', value=82, inplace=True)
test_data['category'].replace(to_replace='R&B', value=82, inplace=True)

train_data['category'].replace(to_replace='Country & Folk', value=83, inplace=True)
test_data['category'].replace(to_replace='Country & Folk', value=83, inplace=True)

train_data['category'].replace(to_replace='Latin', value=84, inplace=True)
test_data['category'].replace(to_replace='Latin', value=84, inplace=True)

train_data['category'].replace(to_replace='Ready-to-wear', value=85, inplace=True)
test_data['category'].replace(to_replace='Ready-to-wear', value=85, inplace=True)

train_data['category'].replace(to_replace='Installations', value=86, inplace=True)
test_data['category'].replace(to_replace='Installations', value=86, inplace=True)

train_data['category'].replace(to_replace='DIY Electronics', value=87, inplace=True)
test_data['category'].replace(to_replace='DIY Electronics', value=87, inplace=True)

train_data['category'].replace(to_replace='Periodicals', value=88, inplace=True)
test_data['category'].replace(to_replace='Periodicals', value=88, inplace=True)

train_data['category'].replace(to_replace='Drinks', value=89, inplace=True)
test_data['category'].replace(to_replace='Drinks', value=89, inplace=True)

train_data['category'].replace(to_replace='Community Gardens', value=90, inplace=True)
test_data['category'].replace(to_replace='Community Gardens', value=90, inplace=True)

train_data['category'].replace(to_replace='Faith', value=91, inplace=True)
test_data['category'].replace(to_replace='Faith', value=91, inplace=True)

train_data['category'].replace(to_replace='Horror', value=92, inplace=True)
test_data['category'].replace(to_replace='Horror', value=92, inplace=True)

train_data['category'].replace(to_replace='Vegan', value=93, inplace=True)
test_data['category'].replace(to_replace='Vegan', value=93, inplace=True)

train_data['category'].replace(to_replace='Gaming Hardware', value=94, inplace=True)
test_data['category'].replace(to_replace='Gaming Hardware', value=94, inplace=True)

train_data['category'].replace(to_replace='Journalism', value=95, inplace=True)
test_data['category'].replace(to_replace='Journalism', value=95, inplace=True)

train_data['category'].replace(to_replace='Science Fiction', value=96, inplace=True)
test_data['category'].replace(to_replace='Science Fiction', value=96, inplace=True)

train_data['category'].replace(to_replace='Festivals', value=97, inplace=True)
test_data['category'].replace(to_replace='Festivals', value=97, inplace=True)

train_data['category'].replace(to_replace='Jazz', value=98, inplace=True)
test_data['category'].replace(to_replace='Jazz', value=98, inplace=True)

train_data['category'].replace(to_replace='Animals', value=99, inplace=True)
test_data['category'].replace(to_replace='Animals', value=99, inplace=True)

train_data['category'].replace(to_replace='DIY', value=100, inplace=True)
test_data['category'].replace(to_replace='DIY', value=100, inplace=True)

train_data['category'].replace(to_replace='Young Adult', value=101, inplace=True)
test_data['category'].replace(to_replace='Young Adult', value=101, inplace=True)

train_data['category'].replace(to_replace='Sound', value=102, inplace=True)
test_data['category'].replace(to_replace='Sound', value=102, inplace=True)

train_data['category'].replace(to_replace='Graphic Design', value=103, inplace=True)
test_data['category'].replace(to_replace='Graphic Design', value=103, inplace=True)

train_data['category'].replace(to_replace='Punk', value=104, inplace=True)
test_data['category'].replace(to_replace='Punk', value=104, inplace=True)

train_data['category'].replace(to_replace='Musical', value=105, inplace=True)
test_data['category'].replace(to_replace='Musical', value=105, inplace=True)

train_data['category'].replace(to_replace='Candles', value=106, inplace=True)
test_data['category'].replace(to_replace='Candles', value=106, inplace=True)

train_data['category'].replace(to_replace='Live Games', value=107, inplace=True)
test_data['category'].replace(to_replace='Live Games', value=107, inplace=True)

train_data['category'].replace(to_replace='Family', value=108, inplace=True)
test_data['category'].replace(to_replace='Family', value=108, inplace=True)

train_data['category'].replace(to_replace='Graphic Novels', value=109, inplace=True)
test_data['category'].replace(to_replace='Graphic Novels', value=109, inplace=True)

train_data['category'].replace(to_replace='Farms', value=110, inplace=True)
test_data['category'].replace(to_replace='Farms', value=110, inplace=True)

train_data['category'].replace(to_replace='Fantasy', value=111, inplace=True)
test_data['category'].replace(to_replace='Fantasy', value=111, inplace=True)

train_data['category'].replace(to_replace='Wearables', value=112, inplace=True)
test_data['category'].replace(to_replace='Wearables', value=112, inplace=True)

train_data['category'].replace(to_replace='Printing', value=113, inplace=True)
test_data['category'].replace(to_replace='Printing', value=114, inplace=True)

train_data['category'].replace(to_replace='Woodworking', value=115, inplace=True)
test_data['category'].replace(to_replace='Woodworking', value=115, inplace=True)

train_data['category'].replace(to_replace='Academic', value=116, inplace=True)
test_data['category'].replace(to_replace='Academic', value=116, inplace=True)

train_data['category'].replace(to_replace='Places', value=117, inplace=True)
test_data['category'].replace(to_replace='Places', value=117, inplace=True)

train_data['category'].replace(to_replace='Blues', value=118, inplace=True)
test_data['category'].replace(to_replace='Blues', value=118, inplace=True)

train_data['category'].replace(to_replace='Couture', value=119, inplace=True)
test_data['category'].replace(to_replace='Couture', value=119, inplace=True)

train_data['category'].replace(to_replace='Flight', value=120, inplace=True)
test_data['category'].replace(to_replace='Flight', value=120, inplace=True)

train_data['category'].replace(to_replace='Movie Theaters', value=121, inplace=True)
test_data['category'].replace(to_replace='Movie Theaters', value=121, inplace=True)

train_data['category'].replace(to_replace='Events', value=122, inplace=True)
test_data['category'].replace(to_replace='Events', value=122, inplace=True)

train_data['category'].replace(to_replace='Weaving', value=123, inplace=True)
test_data['category'].replace(to_replace='Weaving', value=123, inplace=True)

train_data['category'].replace(to_replace='Fine Art', value=124, inplace=True)
test_data['category'].replace(to_replace='Fine Art', value=124, inplace=True)

train_data['category'].replace(to_replace='Action', value=125, inplace=True)
test_data['category'].replace(to_replace='Action', value=125, inplace=True)

train_data['category'].replace(to_replace='Workshops', value=126, inplace=True)
test_data['category'].replace(to_replace='Workshops', value=126, inplace=True)

train_data['category'].replace(to_replace='Textiles', value=127, inplace=True)
test_data['category'].replace(to_replace='Textiles', value=127, inplace=True)

train_data['category'].replace(to_replace='Knitting', value=128, inplace=True)
test_data['category'].replace(to_replace='Knitting', value=128, inplace=True)

train_data['category'].replace(to_replace='Literary Journals', value=129, inplace=True)
test_data['category'].replace(to_replace='Literary Journals', value=129, inplace=True)

train_data['category'].replace(to_replace='Cookbooks', value=130, inplace=True)
test_data['category'].replace(to_replace='Cookbooks', value=130, inplace=True)

train_data['category'].replace(to_replace='Camera Equipment', value=131, inplace=True)
test_data['category'].replace(to_replace='Camera Equipment', value=131, inplace=True)

train_data['category'].replace(to_replace='Stationery', value=132, inplace=True)
test_data['category'].replace(to_replace='Stationery', value=132, inplace=True)

train_data['category'].replace(to_replace='Performances', value=133, inplace=True)
test_data['category'].replace(to_replace='Performances', value=133, inplace=True)

train_data['category'].replace(to_replace='Civic Design', value=134, inplace=True)
test_data['category'].replace(to_replace='Civic Design', value=134, inplace=True)

train_data['category'].replace(to_replace='Childrenswear', value=135, inplace=True)
test_data['category'].replace(to_replace='Childrenswear', value=135, inplace=True)

train_data['category'].replace(to_replace='3D Printing', value=136, inplace=True)
test_data['category'].replace(to_replace='3D Printing', value=136, inplace=True)

train_data['category'].replace(to_replace='Television', value=137, inplace=True)
test_data['category'].replace(to_replace='Television', value=137, inplace=True)

train_data['category'].replace(to_replace='Bacon', value=138, inplace=True)
test_data['category'].replace(to_replace='Bacon', value=138, inplace=True)

train_data['category'].replace(to_replace='Pottery', value=139, inplace=True)
test_data['category'].replace(to_replace='Pottery', value=139, inplace=True)

train_data['category'].replace(to_replace='Pet Fashion', value=140, inplace=True)
test_data['category'].replace(to_replace='Pet Fashion', value=140, inplace=True)

train_data['category'].replace(to_replace='Romance', value=141, inplace=True)
test_data['category'].replace(to_replace='Romance', value=141, inplace=True)

train_data['category'].replace(to_replace='Immersive', value=142, inplace=True)
test_data['category'].replace(to_replace='Immersive', value=142, inplace=True)

train_data['category'].replace(to_replace='Nature', value=143, inplace=True)
test_data['category'].replace(to_replace='Nature', value=143, inplace=True)

train_data['category'].replace(to_replace='Ceramics', value=144, inplace=True)
test_data['category'].replace(to_replace='Ceramics', value=144, inplace=True)

train_data['category'].replace(to_replace='Robots', value=145, inplace=True)
test_data['category'].replace(to_replace='Robots', value=145, inplace=True)

train_data['category'].replace(to_replace='Photo', value=146, inplace=True)
test_data['category'].replace(to_replace='Photo', value=146, inplace=True)

train_data['category'].replace(to_replace='Fabrication Tools', value=147, inplace=True)
test_data['category'].replace(to_replace='Fabrication Tools', value=147, inplace=True)

train_data['category'].replace(to_replace='Video', value=148, inplace=True)
test_data['category'].replace(to_replace='Video', value=148, inplace=True)

train_data['category'].replace(to_replace='Kids', value=149, inplace=True)
test_data['category'].replace(to_replace='Kids', value=149, inplace=True)

train_data['category'].replace(to_replace='Space Exploration', value=150, inplace=True)
test_data['category'].replace(to_replace='Space Exploration', value=150, inplace=True)

train_data['category'].replace(to_replace='Puzzles', value=151, inplace=True)
test_data['category'].replace(to_replace='Puzzles', value=151, inplace=True)

train_data['category'].replace(to_replace='Residencies', value=152, inplace=True)
test_data['category'].replace(to_replace='Residencies', value=152, inplace=True)

train_data['category'].replace(to_replace='Typography', value=153, inplace=True)
test_data['category'].replace(to_replace='Typography', value=153, inplace=True)

train_data['category'].replace(to_replace='Quilts', value=154, inplace=True)
test_data['category'].replace(to_replace='Quilts', value=154, inplace=True)

train_data['category'].replace(to_replace='Crochet', value=155, inplace=True)
test_data['category'].replace(to_replace='Crochet', value=155, inplace=True)

train_data['category'].replace(to_replace='Taxidermy', value=156, inplace=True)
test_data['category'].replace(to_replace='Taxidermy', value=156, inplace=True)

train_data['category'].replace(to_replace='Letterpress', value=157, inplace=True)
test_data['category'].replace(to_replace='Letterpress', value=157, inplace=True)

train_data['category'].replace(to_replace='Chiptune', value=158, inplace=True)
test_data['category'].replace(to_replace='Chiptune', value=158, inplace=True)

train_data['category'].replace(to_replace='Literary Spaces', value=159, inplace=True)
test_data['category'].replace(to_replace='Literary Spaces', value=159, inplace=True)

train_data['category'].replace(to_replace='Art', value=160, inplace=True)
test_data['category'].replace(to_replace='Art', value=160, inplace=True)

train_data.head()




X = train_data.drop(['state','ID', 'name', 'goal', 'pledged', 'usd_pledged_real'], axis = 'columns').values
y = train_data['state'].values

X2 = test_data.drop(['state','ID', 'name', 'goal', 'pledged', 'usd_pledged_real'], axis = 'columns').values
y2 = test_data['state'].values
X.shape




#запускаем ГБ
from sklearn import decomposition
from sklearn.preprocessing import StandardScaler 

ss = StandardScaler()

pca = decomposition.PCA(n_components=2, random_state=228) #уменьшаем пространство признаков до 2
pca.fit(X)

X_2d = pca.transform(X) #унифицируем их (числа)
X2_2d = pca.transform(X2)

X_2d = ss.fit_transform(X_2d)
X2_2d = ss.transform(X2_2d)




get_ipython().run_line_magic('matplotlib', 'inline')
#_____________Входные данные и метки
def draw_objects(X, y, bdt): #функция, которая будет рисовать объекты
    X_green_list = []
    X_red_list = []
    for i in range(X.shape[0]):
        if (y[i] > 0.5):
            X_green_list.append(X[i, :])
        else:
            X_red_list.append(X[i, :])
    X_green = np.array(X_green_list)
    X_red = np.array(X_red_list)
    plt.figure(figsize=(12, 12))
    plt.plot(X_green[:, 0], X_green[:, 1], 'ro')
    plt.plot(X_red[:, 0], X_red[:, 1], 'bo')
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    plt.axis([x_min, x_max, y_min, y_max])
    plot_colors = "rg"
    plot_step = 0.02
    xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                         np.arange(y_min, y_max, plot_step))
    Z = bdt.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    cs = plt.contourf(xx, yy, Z, cmap=plt.cm.Paired)
    plt.show()




#запускаем пни
from sklearn.ensemble import RandomForestClassifier
rf = RandomForestClassifier(n_estimators = 300, bootstrap = True, max_depth=8, random_state=228)
rf = rf.fit(X_2d, y)

from sklearn.metrics import accuracy_score
pred = rf.predict(X2_2d)
acc = accuracy_score(y2, pred)
print('Acc: ', acc)




draw_objects(X_2d, y, rf)